#! usr/bin/en python

from nose.tools import *
import aims

#Test function for the std()

def test_ints(): #Test it for a list of integer 
    obs=aims.std([1,2,3,4])
    exp=1.118033989
    assert_almost_equal(obs,exp)
    
def test_empty():#Test it for an empty list
    obs=aims.std([])
    exp=0
    assert_equal(obs,exp)
    
def test_float():#Test it for a list of float
    obs=aims.std([2.0,1.5,3.5,4.1])
    exp=1.061543687
    assert_almost_equal(obs,exp)

def test_infinit():#Test it for a very great value
    obs=aims.std([5,1e424,2,3,1e325])
    exp=NotImplemented
    assert_equal(obs,exp)
    
def test_zero():#Test it for list of zero value
    obs=aims.std([0.0,0.0,0.0,0.0,0.0])
    exp=0.0
    assert_equal(obs,exp)

#Test the accuracy
def test_accuracy():
    obs =aims.std([0.0, 2.0])
    exp = 1.0
    assert_equal(obs, exp)

def test_accuracy1():
    obs = aims.std([1.0, 1.0, 1.0,1.0,1.0])
    exp = 0.0
    assert_equal(obs, exp)

#Test how the program handle the error
def test_std_TypeError():
    assert_raises(TypeError,aims.std,2)

def test_std_TypeError2():
    assert_raises(TypeError,aims.std,[2,"three"])

#####From here the test function is for the avg_range() 
    
def test_avg_2_file():
    obs =aims.avg_range(['data/bert/audioresult-00215','data/bert/audioresult-00222'])
    exp=5
    assert_equal(obs,exp)

def test_avg_morethan2():#Test it when there is more than two file
    obs = aims.avg_range(['data/frank_richard/data_212','data/frank_richard/data_221','data/frank_richard/data_224'])
    exp=7.0
    assert_almost_equal(obs,exp)

def test_avg_floatoutput():#Test when the output must be a float
    obs=aims.avg_range(['data/jamesm/data_217.txt','data/jamesm/data_266.txt','data/jamesm/data_394.txt','data/jamesm/data_478.txt'])
    exp=7.5
    assert_equal(obs,exp)

#Test how the program handle the error
def test_avg_TypeError():
    assert_raises(TypeError,aims.avg_range,2)

def test_avg_IOError():
    assert_raises(IOError,aims.avg_range,["/data/bert","/data/234"])

