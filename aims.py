#! /usr/bin/env python
# The module AIMS
import math

def main():
    print "Welcome to the AIMS module" #To be printed if run in a command line 
    
def std(my_list):
    N=len(my_list)
    try:
        if N==0:
            deviation=0 #we make the deviation equal to zero if it's an empty list
        else:#we compute the deviation
            x_bar=0
            for x in my_list:
                x_bar=x_bar+x
            x_bar=float(x_bar)/N #the mean of the list
            if x_bar >= 1e500:
                return NotImplemented #if the compute value is to big,we stop
            Total=0
            for x in my_list:
                Total=Total + (x-x_bar)**2
            deviation=math.sqrt(float(Total)/N) #The deviation
    except TypeError:# To prevent wrong input
        raise TypeError("The input must be a list of integer or float")
    return deviation

def avg_range(list_Of_Filename):
    try:
        if list_Of_Filename == []:
            print "You have entered an empty list of filename.No average can be compute"
        else:
            average=0
            for filename in list_Of_Filename:
                data = open(filename)#open each file name in the list
                for line in data:
                    my_line=line.strip().split(": ") # get read of the character "\n" then split the file into a list
                    if my_line[0]=="Range": 
                        average =average +float(my_line[1]) 
                data.close()
            average=float(average)/len(list_Of_Filename)
            return average
    #to prevent a wrong input
    except TypeError as detail:
        msg="You must enter a list of filename(path)"
        raise TypeError(detail.__str__() + "\n"+msg)
    except IOError as more_detail:
        msg="You must enter a list of filename(path)"
        raise IOError(more_detail.__str__() +"\n"+msg)
if __name__=="__main__":
    main()
   

